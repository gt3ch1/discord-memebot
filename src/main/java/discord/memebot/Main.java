package discord.memebot;

import javax.security.auth.login.LoginException;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDABuilder;

public class Main {
	public static void main(String[] args) throws LoginException {
		JDABuilder builder = new JDABuilder(AccountType.BOT);
		/*
		 * NOTE:  The "Token" class contains the token for the bot,
		 * and thus is left out from git.  If you want to build your
		 * own memebot, Create a class in this same package called "Token"
		 * and have a public static string named token for this to work.
		 * 
		 */
		String token = Token.token;
		builder.setToken(token);
		// Sets the command class to be the even listener.
		builder.addEventListener(new Commands());
		// Fires up the memebot.
		builder.buildAsync();
	}
}
