package discord.memebot;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

/**
 * This class is the command event listener for The Memebot.  All parts of the 
 * program that requires an action when a message is recieved belong in here.
 * @author gcpease
 *
 */
public class Commands extends ListenerAdapter {
	boolean uploadMode = true;
	/**
	 * Run this code when a message is recieved
	 */
	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		
		/*
		 * Just demo'ing some code to see how it works.
		 */
		if (isCommand("!ping", event)) {
			
			/*
			 * This part is important, if you have a bot send something that
			 * has a command, it'll loop itself unless this block is in place.
			 */
			if (event.getAuthor().isBot()) {
				return;
			}
			
			sendMessage("Message sent", event);
		}
		
		/*
		 * Possible idea of how to manage uploading memes? Might need/want
		 * to make another method for this.
		 */
		if(isCommandContains(".jpg", event)) {
			MemeDatabaseAPI mdapi = new MemeDatabaseAPI(getCommand(event));
			if(uploadMode)
				mdapi.doUpload();
			mdapi.doCalculation();
			boolean isWeeb = mdapi.getCalculation();
			sendMessage("Meme machine says: " + (isWeeb ? "WEEB" : "Dank."), event);
			System.out.println(isWeeb);
		}
		if(isCommandContains(".png", event)) {
			MemeDatabaseAPI mdapi = new MemeDatabaseAPI(getCommand(event));
			if(uploadMode)
				mdapi.doUpload();
			mdapi.doCalculation();
			boolean isWeeb = mdapi.getCalculation();
			sendMessage("Meme machine says: " + (isWeeb ? "WEEB" : "Dank."), event);
			System.out.println(isWeeb);
		}
	}
	
	/**
	 * Returns a boolean based on whether or not the specified command was
	 * sent in the message.
	 * @param command
	 * @param e
	 * @return
	 */
	public boolean isCommand(String command, MessageReceivedEvent e) {
		return e.getMessage().getContentRaw().equalsIgnoreCase(command);
	}
	
	/**
	 * Returns a boolean based on whether or not the specified command is
	 * contained in the message.
	 * @param command
	 * @param e
	 * @return
	 */
	public boolean isCommandContains(String command, MessageReceivedEvent e) {
		return e.getMessage().getContentRaw().contains(command);
	}
	
	/**
	 * Sends a message to the channel in which e was sent on.
	 * @param message
	 * @param e
	 */
	public void sendMessage(String message, MessageReceivedEvent e) {
		e.getChannel().sendMessage(message).queue();;
	}
	
	
	/**
	 * Returns the message from event e.
	 * @param e
	 * @return the message
	 */
	public String getCommand(MessageReceivedEvent e) {
		return e.getMessage().getContentRaw();
	}
}
