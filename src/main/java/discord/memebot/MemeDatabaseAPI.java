package discord.memebot;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import me.gpease.ml.math.Calculations;

public class MemeDatabaseAPI {
	private String url;
	private boolean calculation = false;
	String fileLocation;

	/**
	 * This class downloads, scores, and then uploads the score to the Meme
	 * Database.
	 * 
	 * @param url
	 */
	public MemeDatabaseAPI(String url) {
		this.url = url;
		doDownload();
	}

	/**
	 * Downloads the given URL to a temporary location, so that the
	 * anti-weeb-meme-machine can score it.
	 * 
	 */
	private void doDownload() {

		/*
		 * TODO: Download the URL to a temp file
		 * TODO: Test.
		 */

		String fileLocation = "tmp.jpg";
		try {
			/*
			 * Borrowed this chunk from SO,
			 * "How to download and save a file from internet using java"
			 */
			URL url = new URL(this.url);
			ReadableByteChannel rbc = Channels.newChannel(url.openStream());
			// Is there a way I can tell if the file is a png or a jpg? 
			// not that it really matters, it should work either way.
			FileOutputStream fos = new FileOutputStream("tmp.jpg");
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.fileLocation = fileLocation;

	}
	//TODO:
	// make a method for doUpload...
	/**
	 * Calls the anti-weeb-meme-machine program to run the calculations.
	 * 
	 * @param fileLocation
	 */
	public void doCalculation() {

		/*
		 * TODO: Run the anti-weeb-meme-machine jar file with the calculate or upload
		 * flag on the given file. TODO: Gotta figure out a way to import the stupid
		 * meme jar file properly.
		 */
		boolean finalCalculation = Calculations.doCalculation(new File(this.fileLocation));
		setCalculation(finalCalculation);
	}
	/**
	 * Calls the anti-weeb-meme-machine program to run the calculations.
	 * 
	 * @param fileLocation
	 * TODO: Overload.
	 */
	public void doUpload() {

		/*
		 * TODO: Run the anti-weeb-meme-machine jar file with the calculate or upload
		 * flag on the given file. TODO: Gotta figure out a way to import the stupid
		 * meme jar file properly.
		 */
		Calculations.doUpload(new File(this.fileLocation));
		boolean finalCalculation = Calculations.doCalculation(new File(this.fileLocation));
		setCalculation(finalCalculation);
	}
	/**
	 * Sets the "calculation" boolean flag
	 * 
	 * @param calculation
	 */
	private void setCalculation(boolean calculation) {
		this.calculation = calculation;
	}

	/**
	 * Returns the "calculation" boolean flag
	 * 
	 * @return
	 */
	public boolean getCalculation() {
		return calculation;
	}
}
